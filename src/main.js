//Фрактал Мандельброта
function mandelbrotIteration (cx, cy, maxIter){
  var x = 0.0, y = 0.0,
      xx = 0, yy = 0, xy = 0,
      i = maxIter;
  while (i-- && xx + yy <= 4){
    xy = x * y;
    xx = x * x;
    yy = y * y;
    x = xx - yy + cx;
    y = xy + xy + cy;
  }
  return maxIter - i;
}
function mandelbrot (canvas, xmin, xmax, ymin, ymax, iterations){
  var width = canvas.width, height = canvas.height,
      context = canvas.getContext('2d'),
      image = context.getImageData(0, 0, width, height),
      pixels = image.data;
  for (var ix = 0; ix < width; ++ix){
    for (var iy = 0; iy < height; ++iy){
      var x = xmin + (xmax - xmin) * ix / (width -1),
          y = ymin + (ymax - ymin) * iy / (height - 1),
          i = mandelbrotIteration(x, y, iterations),
      pixels_position = 4 * (width * iy + ix);
      if (i > iterations){
        pixels[pixels_position] = 0;
        pixels[pixels_position + 1] = 0;
        pixels[pixels_position + 2] = 0;
      }
      else{
        var color = 3 * Math.log(i) / Math.log(iterations - 1.0);
        if (color < 1){
          pixels[pixels_position] = 255 * color;
          pixels[pixels_position + 1] = 0;
          pixels[pixels_position + 2] = 0;
        }
        else if (color < 2){
          pixels[pixels_position] = 255;
          pixels[pixels_position + 1] = 255 * (color - 1);
          pixels[pixels_position + 2] = 0;
        }
        else{
          pixels[pixels_position] = 255;
          pixels[pixels_position + 1] = 255;
          pixels[pixels_position + 2] = 255 * (color - 2);
        }
      }
      pixels[pixels_position + 3] = 255;
    }
  }
  context.putImageData(image, 0, 0);
}



//Фрактал жюлиа
function remap(x, t1, t2, s1, s2){
  var f = (x - t1) / (t2 - t1),
      g = f * (s2 - s1) + s1;
  return g;
}
function getColor(c){
  var r, g, b, p = c / 32,
      l = ~~(p * 6), o = p * 6 - l, q = 1 - o;
  switch(l % 6){
    case 0: r = 1; g = o; b = 0; break;
    case 1: r = q; g = 1; b = 0; break;
    case 2: r = 0; g = 1; b = o; break;
    case 3: r = 0; g = q; b = 1; break;
    case 4: r = o; g = 0; b = 1; break;
    case 5: r = 1; g = 0; b = q; break;
  }
  var c = "#" + ("00" + (~~(r*255)).toString(16)).slice(-2) +
                ("00" + (~~(g*255)).toString(16)).slice(-2) +
                ("00" + (~~(b*255)).toString(16)).slice(-2);
  return (c);
}
function julia(canvas, minX, maxX, minY, maxY, jsX, jsY, maxIteration){
  ctx = canvas2.getContext('2d');
  var a, as, za, b ,bs, zb, cnt;
  for (var j = 0; j < canvas2.height; j++){
    for (var i = 0; i < canvas2.width; i++){
      a = remap(i, 0, canvas2.width, minX, maxX);
      b = remap(j, 0, canvas2.height, minY, maxY);
      cnt = 0;
      while (++cnt < maxIteration){
        za = a * a; zb = b * b;
        if (za + zb > 4){ break; }
        as = za - zb; bs = 2 * a * b;
        a = as + jsX; b = bs + jsY;
      }
      if (cnt < maxIteration){
        ctx.fillStyle = getColor(cnt);
      }
      ctx.fillRect(i, j, 1, 1);
    }
  }
}



//фрактал Ньютона
(function() {
  function Complex(r, i) {
      this.r = r;
      this.i = i;
  }
  Complex.prototype.add = function(other) {
      return new Complex(this.r + other.r, this.i + other.i);
  }
  Complex.prototype.sub = function(other) {
      return new Complex(this.r - other.r, this.i - other.i);
  }
  Complex.prototype.mul = function(other) {
      return new Complex(this.r * other.r - this.i * other.i,
                     this.i * other.r + this.r * other.i);
  }
  Complex.prototype.div = function(other) {
      var denominator = other.r * other.r + other.i * other.i;
      return new Complex((this.r * other.r + this.i * other.i) / denominator,
                     (this.i * other.r - this.r * other.i) / denominator);
  }

  function setFillStyle(ctx, r, g, b, a) {
      ctx.fillStyle = "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
  }

  window.onload = function() {
      var node = document.getElementById('container');
      var canvas = document.createElement("canvas"),
          ctx = canvas.getContext("2d");    
      node.appendChild(canvas);


      var one = new Complex(1, 0);
      var three = new Complex(3, 0);
      var f = function(z) { return z.mul(z).mul(z).sub(one); };
      var fPrime = function(z) { return three.mul(z).mul(z); };
      var N = function(z) { return z.sub(f(z).div(fPrime(z))); };

      var bottom = Math.floor(canvas.height / 2);
      var top = -bottom;
      var right = Math.floor(canvas.width / 2);
      var left = -right;

      setFillStyle(ctx, 255, 255, 255, 1);
      ctx.fillRect(0, 0, canvas.width, canvas.height);
      for (var x = left - 10; x < right + 10; x++) {
          for (var y = top - 10; y < bottom + 10; y++) {
              var yo = new Complex(x/500, y/500);
              var ayy = N(yo);
              var ayy2 = N(ayy);
              var n = 0;
              while (Math.abs(ayy2.r - ayy.r) > 0.000001) {
                  ayy = ayy2;
                  ayy2 = N(ayy2);
                  n++;
              }
              if (Math.abs(ayy.r - 1) < 0.001) {
                  setFillStyle(ctx, 0, 198, 255, n/20);
              } else if(Math.abs(ayy.r + 0.5) < 0.001) {
                  setFillStyle(ctx, 0, 0, 0, n/20);
              } else {
                  setFillStyle(ctx, 255, 255, 255, 1);
              }
              ctx.fillRect(x - left, y - top, 1, 1);
          }
      }
  }
})();



var node = document.getElementById('container');
//фрактал Мандельброта
var canvas1 = document.createElement('canvas');
node.appendChild(canvas1);
mandelbrot(canvas1, -2.1, 2.1, -2.1, 2.1, 2000);
//фрактал Жюлия
var canvas2 = document.createElement('canvas');
node.appendChild(canvas2);
julia(canvas2, -0.5, 0.5, -0.5, 0.5, 0.285, 0.01, 2000);
//Хороший костыль, который убирает 2 странных канваса
var canvases = document.getElementsByTagName('canvas');
canvases[0].remove();